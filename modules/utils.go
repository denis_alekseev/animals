package modules

// Здесь лежат функции, которые упрощают работу, но вся их функциональность

import (
	"reflect"
	"strings"
)

//
func getStructFields(s interface{}) []string {
	var res []string

	a := reflect.ValueOf(s)
	num := reflect.ValueOf(s).Elem().NumField()
	if a.Kind() != reflect.Ptr {
		panic("Wrong data type")
	}
	for i := 0; i < num; i++ {
		res = append(res, strings.ToLower(reflect.TypeOf(s).Elem().Field(i).Name))
	}
	return res
}

func isContain(x []string, a string) bool {
	for _, i := range x {
		if i == a {
			return true
		}
	}
	return false
}

func runesToString(runes []rune) (outString string) {
	// don't need index so _
	for _, v := range runes {
		outString += string(v)
	}
	return
}
