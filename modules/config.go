package modules

import (
	"encoding/json"
	"io/ioutil"
)

func ReadConfig(filename string) Config {
	var conf Config

	content, err := ioutil.ReadFile(filename)
	if err != nil {
		panic("Problem with reading config file")
	}

	err = json.Unmarshal(content, &conf)
	if err != nil {
		panic(err.Error())
	}

	return conf
}
