package modules

import "testing"

var animalsList AnimalList

func TestAnimalLoading(t *testing.T) {
	defer func() {
		err := recover()
		if err != "Problem with opening file zverugi.json" {
			t.Error(err)
		} else {
			t.Log("Expected panic: ", err)
		}
	}()

	animalsList = NewAnimalList("../animals.json")
	if len(animalsList) == 0 {
		t.Error("Wrong file length")
	}

	NewAnimalList("zverugi.json")
}

func TestAnimalList_Save(t *testing.T) {
	conf := Config{
		DbType:     "mongo",
		Server:     "mongodb://127.0.0.1:27017/testStore",
		Database:   "animals",
		Collection: "animals_test",
	}
	GetRepository(conf)
	animalsList.Save()
}
