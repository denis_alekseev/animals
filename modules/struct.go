package modules

// структура файла конфигурации

type Config struct {
	DbType     string `json:"type"`
	Server     string `json:"server"`
	Database   string `json:"db"`
	Collection string `json:"collection"`
}

/**
Структура, описывающая животное и отображене на JSON
*/
type Animal struct {
	Name   string `json:"name"`
	Weight string `json:"weight"`
	Size   string `json:"size"`
	Type   string `json:"type"`
}

type AnimalList []Animal
