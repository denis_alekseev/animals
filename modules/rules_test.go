package modules

import "testing"

func TestNewRulesList(t *testing.T) {
	rules := NewRulesList("../rules.txt")
	if len(rules) == 0 {
		t.Error("Wrong rules list")
	}
	rules.Run()
}
