package modules

import "testing"

func TestConfig(t *testing.T) {
	defer func() {
		err := recover()
		if err != "Problem with reading config file" {
			t.Error(err)
		} else {
			t.Log("Expected panic: ", err)
		}
	}()

	conf := ReadConfig("../config.json")
	if conf.DbType != "mongo" {
		t.Error("Config reading problem")
	}

	ReadConfig("config.json")
}
