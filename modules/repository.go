package modules

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"sync"
)

type Repository struct {
	Session    *mgo.Session
	Db         string
	Collection string
}

var instance *Repository
var mu sync.Mutex

// По сути здесь мы используем для создания хранилища патерн singleton

func GetRepository(conf Config) *Repository {
	mu.Lock() // на всякий случай сделаем thread-safety
	defer mu.Unlock()
	if instance == nil {
		switch conf.DbType {
		case "mongo":
			instance = makeMongoInstance(conf)
		}
	}

	return instance
}

func makeMongoInstance(conf Config) *Repository {
	instance = &Repository{}
	session, err := mgo.Dial(conf.Server)
	if err != nil {
		panic("Database connection failure")
	}
	instance.Session = session
	instance.Db = conf.Database
	instance.Collection = conf.Collection
	// Если в БД уже есть данные очищаем её. В нашем случае БД используется просто, как способ облегчить себе жизнь
	// И постоянное хранение данных  в ней не обязательно
	count, err := session.DB(conf.Database).C(conf.Collection).Find(bson.M{}).Count()
	if count != 0 {
		_, err = session.DB(conf.Database).C(conf.Collection).RemoveAll(bson.M{})
		if err != nil {
			panic(err.Error())
		}
	}
	return instance
}
