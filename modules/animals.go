package modules

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

func NewAnimalList(filename string, format ...string) AnimalList {

	res := AnimalList{}
	// в зависимости от format может быть добавлен новый формат файла со списком животных по умолчанию используется json
	switch format {
	default:
		res = readAnimalsFromJson(filename)
	}

	return res
}

func readAnimalsFromJson(filename string) AnimalList {
	var res AnimalList

	content, err := ioutil.ReadFile(filename)
	if err != nil {
		panic("Problem with opening file " + filename)
	}

	err = json.Unmarshal(content, &res)
	if err != nil {
		panic(err.Error())
	}

	return res
}

func (al AnimalList) Save() {
	// если прочитанный список пуст просто возвращаемся
	if len(al) == 0 {
		return
	}

	rep := GetRepository(Config{})
	connection := rep.Session.DB(rep.Db).C(rep.Collection)
	for _, animal := range al {
		err := connection.Insert(animal)
		if err != nil {
			fmt.Println(err.Error())
		}
	}
}
