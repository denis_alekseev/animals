package modules

import (
	"bufio"
	"fmt"
	"gopkg.in/mgo.v2/bson"
	"os"
	"strings"
)

type RulesList map[string][]string

// Читаем из файла список правил
func NewRulesList(filename string) RulesList {
	res := make(RulesList)
	file, err := os.Open(filename)
	if err != nil {
		panic("Can't open rules file")
	}
	defer file.Close()

	reader := bufio.NewScanner(file)

	for reader.Scan() {
		str := reader.Text()
		rulesStr := strings.Split(str, ";")
		res[rulesStr[0]] = rulesStr[1:]
	}

	return res
}

// формируем запрос к БД для каждого правила
func (r *RulesList) Run() []string {
	repo := GetRepository(Config{})
	var res []string
	c := repo.Session.DB(repo.Db).C(repo.Collection)
	for name, rules := range *r {
		// Эту часть можно оформить в виде отдельной goroutin-ы и выполнять все запросы параллельно
		filter := r.prepareQuery(rules)
		num, err := c.Find(filter).Count()
		if err != nil {
			panic(err.Error())
		}
		res = append(res, fmt.Sprintf("%s : %d", name, num))

	}
	return res
}

// формируем для каждого поля часть запроса
func (r *RulesList) prepareQuery(fields []string) bson.M {
	res := []bson.M{}
	fieldNames := getStructFields(&Animal{})
	for _, field := range fields {
		f := strings.Split(field, ":")
		// если список вариантов пуст игнорировать
		if len(f) != 2 {
			continue
		}
		// если поля нет в структуре данных Animal выдать сообщение и не добавлять в запрос
		if !isContain(fieldNames, f[0]) {
			fmt.Println("Unknown field name: " + f[0])
			continue
		}

		//преобразуем список параметров в слайс
		vals := strings.Split(f[1], ",")
		if len(vals) > 1 {
			// если у нас список значений подразумеваем, что мы имеем список значений параметра, которые удовлетворяют заданному условию
			res = append(res, bson.M{f[0]: bson.M{"$in": strings.Split(f[1], ",")}})
		} else {
			r := strings.Split(f[1], "")

			if r[0] == "~" {
				// если у нас одно значение и оно начинается с символа ~ считаем, что это означает отрицание
				res = append(res, bson.M{f[0]: bson.M{"$ne": strings.Join(r[1:], "")}})
			} else {
				// иначе просто добавляем условие
				res = append(res, bson.M{f[0]: f[1]})
			}
		}
	}

	result := bson.M{"$and": res}
	return result
}
