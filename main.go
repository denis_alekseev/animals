package main

import (
	"animals/modules"
	"flag"
	"fmt"
	"os"
)

func main() {
	// получаем аргументы из  командной строки
	in := flag.String("input", "", "Path to input json file(Required)")
	rules := flag.String("rules", "", "Path to rules file(Required)")
	flag.Parse()

	if *in == "" || *rules == "" {
		flag.PrintDefaults()
		os.Exit(1)
	}

	// Для прода так лучше не делать и нормально обработать ошибку в месте предполагаемого возникновения
	defer func() {
		if r := recover(); r != nil {
			fmt.Println(r)
		}
	}()

	// читаем конфигурацию
	conf := modules.ReadConfig("config.json")
	// singleton для общения с базой данных
	modules.GetRepository(conf)

	// получаем список животных
	list := modules.NewAnimalList(*in)
	// Сохраняем его в БД
	list.Save()

	//Получаем список правил...
	rulesList := modules.NewRulesList(*rules)
	// ...парсим параметры и выполняем запросы
	res := rulesList.Run()
	// выводим результаты работы
	for _, str := range res {
		fmt.Println(str)
	}
}
